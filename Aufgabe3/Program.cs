﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the MainClass type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            string[] menuItems = { "Show help", "Change game mode", "Start game!", "Exit" };

            string[] gameModeMenu = { "Singleplayer", "Multiplayer", "Back" };

            string[] helpText =
                {
                    "Welcome to Snake!", "In this game, you control a hungry snake that wants to eat a lot.",
                    "Controls:", " Player 1 Up = ↑, Down = ↓, Left = ←, Right = →",
                    " Player 2 Up = W, Down = S, Left = A, Right = D",
                    "Snakes are fragile, if you touch a wall or another snake, the snake will",
                    "die and you lose the game. Survive as long as you can!", "Stuff to eat:",
                    " Fruit [a]: Tasty. Makes your snake longer (and gain 2 points).",
                    " Fly Agaric [F]: Yucky. Makes your snake move slower and",
                    "                 decreases your multiplier.",
                    " Magic Mushroom [M]: Trippy. Makes your snake move faster",
                    "                     and increases your multiplier.", "Other Items:",
                    " Rainbow [R]: Colors your snake as you move through it and gain 10 points.",
                    " Remover [Ω]: Removes the last segment of your snake and 10 of your points too."
                };

            GameMode mode = new SinglePlayer();

            while (true)
            {
                int choice = Ui.ShowMenu("Main Menu", menuItems);

                if (choice == 0)
                {
                    Ui.ShowHelp(helpText);
                }
                else if (choice == 1)
                {
                    switch (Ui.ShowMenu("Change Gamemode", gameModeMenu))
                    {
                        case 0:
                            mode = new SinglePlayer();
                            break;
                        case 1:
                            mode = new MultiPlayer();
                            break;
                    }
                }
                else if (choice == 2)
                {
                    Game game = new Game(mode);
                    game.Run();
                }
                else if (choice == 3)
                {
                    return;
                }
            }
        }
    }
}