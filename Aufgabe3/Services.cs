﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Services.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Services type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Provides global services for the rest of the game.
    /// </summary>
    public class Services
    {
        /// <summary>
        /// Initializes static members of the <see cref="Services"/> class.
        /// </summary>
        static Services()
        {
            Graphics = new ConsoleRenderer();
            Input = new KeyboardInput();
        }

        /// <summary>
        /// Gets the current graphics adapter.
        /// </summary>
        public static IRenderer Graphics { get; }

        /// <summary>
        /// Gets the current input manager.
        /// </summary>
        public static IInputManager Input { get; }
    }
}