﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColorCycleComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the ColorCycleComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Cycles the entities color.
    /// </summary>
    public class ColorCycleComponent : GameComponent
    {
        /// <summary>
        /// How long to wait before cycling color again.
        /// </summary>
        private readonly int waitTime;

        /// <summary>
        /// The timer.
        /// </summary>
        private int timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorCycleComponent"/> class.
        /// </summary>
        /// <param name="speed">
        /// How fast to cycle.
        /// </param>
        public ColorCycleComponent(int speed = 5)
        {
            this.timer = 0;
            this.waitTime = speed;
        }

        /// <inheritdoc />
        public override void Draw(IRenderer graphics)
        {
        }

        /// <inheritdoc />
        public override void HandleEvent(GameEvent theEvent)
        {
        }

        /// <inheritdoc />
        public override void Update()
        {
            this.timer++;

            if (this.timer > this.waitTime)
            {
                this.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeColor, Utilities.GetRandomColor()));
                this.timer = 0;
            }
        }
    }
}