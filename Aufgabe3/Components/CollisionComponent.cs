﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollisionComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the CollisionComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Provides a collision box between entities.
    /// </summary>
    public class CollisionComponent : GameComponent, ICollider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CollisionComponent"/> class.
        /// </summary>
        /// <param name="passable">
        /// Wheter the entity is passable.
        /// </param>
        public CollisionComponent(bool passable)
        {
            this.Passable = passable;
        }

        /// <summary>
        /// Gets a value indicating whether the entity is passable.
        /// </summary>
        public bool Passable { get; }

        /// <summary>
        /// Checks if this entity collides with another.
        /// </summary>
        /// <param name="collider">
        /// The collider.
        /// </param>
        /// <returns>
        /// The result of the check.
        /// </returns>
        public bool DoesCollide(Entity collider)
        {
            if (collider.Position.Equals(this.Owner.Position) && collider != this.Owner)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Draw this component.
        /// </summary>
        /// <param name="graphics">
        /// The graphics.
        /// </param>
        public override void Draw(IRenderer graphics)
        {
        }

        /// <summary>
        /// Handle events.
        /// </summary>
        /// <param name="theEvent">
        /// The the event.
        /// </param>
        public override void HandleEvent(GameEvent theEvent)
        {
        }

        /// <summary>
        /// Called when the owning entity collided with something.
        /// </summary>
        /// <param name="collider">What the entity collided with.</param>
        /// <param name="position">Where the collision happened.</param>
        public void OnCollide(Entity collider, Vector2 position)
        {
        }

        /// <summary>
        /// Update this component.
        /// </summary>
        public override void Update()
        {
        }
    }
}