﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicMovementComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the MainClass type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using Aufgabe3.Interfaces;

    /// <summary>
    /// The basic movement component.
    /// </summary>
    public class BasicMovementComponent : GameComponent
    {
        /// <summary>
        /// The lowest speed possible.
        /// </summary>
        private const int LowerLimit = 10;

        /// <summary>
        /// The fastest speed possible.
        /// </summary>
        private const int UpperLimit = 20;

        /// <summary>
        /// The timer.
        /// </summary>
        private int timer;

        /// <summary>
        /// Time to wait until the entity moves.
        /// </summary>
        private int waitTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicMovementComponent"/> class.
        /// </summary>
        /// <param name="speed">
        /// The speed.
        /// </param>
        public BasicMovementComponent(int speed)
        {
            this.timer = 0;
            this.waitTime = speed;

            this.SubscribedEvents.Add(EventType.SpeedDown);
            this.SubscribedEvents.Add(EventType.SpeedUp);
        }

        /// <summary>
        /// Draw this component.
        /// </summary>
        /// <param name="graphics">
        /// The graphics.
        /// </param>
        public override void Draw(IRenderer graphics)
        {
        }

        /// <summary>
        /// Handle events.
        /// </summary>
        /// <param name="theEvent">
        /// The the event.
        /// </param>
        public override void HandleEvent(GameEvent theEvent)
        {
            switch (theEvent.Type)
            {
                case EventType.SpeedUp:
                    if (this.waitTime > LowerLimit)
                    {
                        this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeMultiplier, 1));
                        this.waitTime--;
                    }

                    break;
                case EventType.SpeedDown:
                    if (this.waitTime < UpperLimit)
                    {
                        this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeMultiplier, -1));
                        this.waitTime++;
                    }

                    break;
            }
        }

        /// <summary>
        /// Update this component.
        /// </summary>
        public override void Update()
        {
            this.timer++;

            if (this.timer >= this.waitTime)
            {
                this.timer = 0;

                this.Owner.Position = this.Owner.Position.Add(this.Owner.Velocity);
                this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeScore, 1));
                this.BroadcastEvent(new GameEvent(this.Owner, EventType.EntityWasMoved, this.Owner.Position));
            }
        }
    }
}