﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DurabilityComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the DurabilityComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using System.Diagnostics;

    using Aufgabe3.Interfaces;

    /// <summary>
    /// Implements an HP system. Entities can take damage and are destroyed once their life reaches 0.
    /// </summary>
    public class DurabilityComponent : GameComponent
    {
        /// <summary>
        /// The durability of the entity.
        /// </summary>
        private int durability;

        /// <summary>
        /// Initializes a new instance of the <see cref="DurabilityComponent"/> class.
        /// </summary>
        /// <param name="durability">
        /// How much HP the entity has.
        /// </param>
        public DurabilityComponent(int durability)
        {
            this.durability = durability;
            this.SubscribedEvents.Add(EventType.TakeDamage);
        }

        /// <inheritdoc />
        public override void Draw(IRenderer graphics)
        {
        }

        /// <inheritdoc />
        public override void HandleEvent(GameEvent theEvent)
        {
            if (theEvent.Type == EventType.TakeDamage)
            {
                Debug.WriteLine($"{this.Owner} takes {(int)theEvent.Data} damage");

                this.durability -= (int)theEvent.Data;

                if (this.durability <= 0)
                {
                    this.Owner.Destroy();
                }
            }
        }

        /// <inheritdoc />
        public override void Update()
        {
        }
    }
}