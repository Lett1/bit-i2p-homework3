﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the CharacterComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using System;

    using Aufgabe3.Interfaces;

    /// <summary>
    /// The character component. Draws a character to the screen at the entities position.
    /// </summary>
    public class CharacterComponent : GameComponent
    {
        /// <summary>
        /// The color to use when drawing the component.
        /// </summary>
        private ConsoleColor color;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Aufgabe3.Components.CharacterComponent" /> class.
        /// </summary>
        /// <param name="character">
        /// The character to draw.
        /// </param>
        /// <param name="color">
        /// The color to use.
        /// </param>
        public CharacterComponent(char character, ConsoleColor color = ConsoleColor.White)
        {
            this.SubscribedEvents.Add(EventType.ChangeColor);
            this.SubscribedEvents.Add(EventType.ChangeSymbol);

            this.Character = character;
            this.color = color;
        }

        /// <summary>
        /// Gets or sets the character.
        /// </summary>
        public char Character { get; set; }

        /// <summary>
        /// Gets or sets the color of the character.
        /// </summary>
        public ConsoleColor Color { get; set; }

        /// <summary>
        /// Draw this component.
        /// </summary>
        /// <param name="graphics">
        /// The graphics.
        /// </param>
        public override void Draw(IRenderer graphics)
        {
            ConsoleColor old = graphics.GetForegroundColor();
            graphics.SetForegroundColor(this.color);
            graphics.PutChar(this.Owner.Position.X, this.Owner.Position.Y, this.Character);
            graphics.SetForegroundColor(old);
        }

        /// <summary>
        /// Handle events.
        /// </summary>
        /// <param name="theEvent">
        /// The the event.
        /// </param>
        public override void HandleEvent(GameEvent theEvent)
        {
            if (theEvent.Type == EventType.ChangeSymbol)
            {
                char newSymbol = (char)theEvent.Data;
                this.Character = newSymbol;
            }
            else if (theEvent.Type == EventType.ChangeColor)
            {
                this.color = (ConsoleColor)theEvent.Data;
            }
        }

        /// <summary>
        /// Update this component.
        /// </summary>
        public override void Update()
        {
        }
    }
}