﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TailComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the TailSegment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using System;
    using System.Collections.Generic;

    using Aufgabe3.Enums;
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Draws a tail behind the entity. Mostly used for the snake.
    /// </summary>
    public class TailComponent : GameComponent, ICollider
    {
        /// <summary>
        /// The segments of the tail.
        /// </summary>
        private List<TailSegment> segments;

        /// <summary>
        /// Initializes a new instance of the <see cref="TailComponent"/> class.
        /// </summary>
        public TailComponent()
        {
            this.SubscribedEvents.Add(EventType.EntityWasMoved);
            this.SubscribedEvents.Add(EventType.GainTail);
            this.SubscribedEvents.Add(EventType.LoseTail);
            this.SubscribedEvents.Add(EventType.ChangeColor);

            this.segments =
                new List<TailSegment> { new TailSegment(new Vector2(0, 0)), new TailSegment(new Vector2(0, 0)) };
        }

        /// <summary>
        /// Check if we hit one of the tail segments.
        /// </summary>
        /// <param name="collider">The collider.</param>
        /// <returns>Boolean indicating whether we hit something.</returns>
        public bool DoesCollide(Entity collider)
        {
            for (int i = 0; i < this.segments.Count - 1; i++)
            {
                var segment = this.segments[i];
                if (segment.Position.Equals(collider.Position))
                {
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc />
        public override void Draw(IRenderer graphics)
        {
            for (int i = 0; i < this.segments.Count - 1; i++)
            {
                var segment = this.segments[i];
                Console.SetCursorPosition(segment.Position.X, segment.Position.Y);
                Console.ForegroundColor = segment.Color;
                Console.Write("+");
            }
        }

        /// <inheritdoc />
        public override void HandleEvent(GameEvent theEvent)
        {
            switch (theEvent.Type)
            {
                case EventType.EntityWasMoved:
                    var position = (Vector2)theEvent.Data;

                    var head = this.segments[this.segments.Count - 1];
                    head = new TailSegment(head.Position, head.Color);
                    this.segments.RemoveAt(0);

                    head.Position = position;
                    this.segments.Add(head);
                    break;
                case EventType.GainTail:
                    this.segments.Add(new TailSegment(this.Owner.Position));
                    this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeSnakeLength, 1));
                    break;
            }
        }

        /// <inheritdoc />
        public void OnCollide(Entity collider, Vector2 position)
        {
            // Kill the snake if we hit our own tail or a wall
            if ((collider == this.Owner && this.GetHitSegment(collider.Position) < this.segments.Count)
                || collider == null)
            {
                this.Owner.Destroy();
                return;
            }

            // Color the segments as they move through the rainbow
            if (collider.Type == EntityType.Colorizer)
            {
                for (var index = 0; index < this.segments.Count; index++)
                {
                    var segment = this.segments[index];
                    if (segment.Position.Equals(position))
                    {
                        this.segments[index] = new TailSegment(
                            segment.Position,
                            (ConsoleColor)Utilities.Random.Next(1, 16));
                    }
                }
            }
            else if (collider.Type == EntityType.Remover) 
            {
                // Remove the last segment of the tail once we moved through a Remover
                if (this.GetHitSegment(collider.Position) == 0)
                {
                    this.segments.RemoveAt(0);
                    this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeSnakeLength, -1));
                }

                collider.Events.Push(new GameEvent(this.Owner, EventType.TakeDamage, 1));
            }
        }

        /// <inheritdoc />
        public override void Update()
        {
        }

        /// <summary>
        /// Find which segment has been hit.
        /// </summary>
        /// <param name="hitPosition">
        /// The hit position.
        /// </param>
        /// <returns>
        /// The index of the hit segment.
        /// </returns>
        private int GetHitSegment(Vector2 hitPosition)
        {
            // TODO: Redo this without findindex
            return this.segments.FindIndex(segment => segment.Position.Equals(hitPosition));
        }
    }
}