﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RandomDirectionComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the RandomDirectionComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Sets the velocity of the entity to a random direction.
    /// </summary>
    public class RandomDirectionComponent : GameComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RandomDirectionComponent"/> class.
        /// </summary>
        public RandomDirectionComponent()
        {
            this.SubscribedEvents.Add(EventType.EntityWasMoved);
        }

        /// <inheritdoc />
        public override void Draw(IRenderer graphics)
        {
        }

        /// <inheritdoc />
        public override void HandleEvent(GameEvent theEvent)
        {
            if (theEvent.Type == EventType.EntityWasMoved)
            {
                switch (Utilities.Random.Next(0, 4))
                {
                    case 0:
                        this.Owner.Velocity.X = 1;
                        break;
                    case 1:
                        this.Owner.Velocity.X = -1;
                        break;
                    case 2:
                        this.Owner.Velocity.Y = 1;
                        break;
                    case 3:
                        this.Owner.Velocity.Y = -1;
                        break;
                }
            }
        }

        /// <inheritdoc />
        public override void Update()
        {
        }
    }
}