﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SnakeColliderComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the SnakeColliderComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using Aufgabe3.Entities;
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Implements the collision logic for a snake.
    /// </summary>
    public class SnakeColliderComponent : GameComponent, ICollider
    {
        /// <inheritdoc />
        public bool DoesCollide(Entity collider)
        {
            if (collider.Position.Equals(this.Owner.Position) && collider != this.Owner)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        public override void Draw(IRenderer graphics)
        {
        }

        /// <inheritdoc />
        public override void HandleEvent(GameEvent theEvent)
        {
        }

        /// <inheritdoc />
        public void OnCollide(Entity collider, Vector2 position)
        {
            if (collider is Fruit)
            {
                // We hit a fruit, so we grow the snake by one segment.
                this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeScore, 2));
                this.BroadcastEvent(new GameEvent(this.Owner, EventType.GainTail, null));

                // Destroy the fruit.
                collider.Destroy();
            }
            else if (collider is Colorizer)
            {
                // Gain 10 Points and set the snakes head to a random color.
                this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeScore, 10));
                this.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeColor, Utilities.GetRandomColor()));
            }
            else if (collider is Remover)
            {
                // Lose 10 Points.
                this.Owner.Game.BroadcastEvent(new GameEvent(this.Owner, EventType.ChangeScore, -10));
            }
            else if (collider is MagicMushroom)
            {
                // Increase the snakes speed.
                this.BroadcastEvent(new GameEvent(this.Owner, EventType.SpeedUp, null));
                collider.Destroy();
            }
            else if (collider is FlyAgaric)
            {
                // Decease the snakes speed.
                this.BroadcastEvent(new GameEvent(this.Owner, EventType.SpeedDown, null));
                collider.Destroy();
            }
            else if (collider == null)
            {
                // We hit a wall. This kills the snake.
                this.Owner.Destroy();
            }
        }

        /// <inheritdoc />
        public override void Update()
        {
        }
    }
}