﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyboardInputComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the KeyboardInputComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using System;

    using Aufgabe3.Enums;
    using Aufgabe3.Interfaces;

    /// <summary>
    /// Reads keyboard input and sets the entities velocity in the corresponding direction.
    /// </summary>
    public class KeyboardInputComponent : GameComponent
    {
        /// <summary>
        /// The key mapping.
        /// </summary>
        private ConsoleKey[] keyMapping;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardInputComponent"/> class.
        /// </summary>
        /// <param name="keyMapping">
        /// The key mapping to use (U D L R).
        /// </param>
        public KeyboardInputComponent(ConsoleKey[] keyMapping)
        {
            this.keyMapping = keyMapping;
        }

        /// <inheritdoc />
        public override void Draw(IRenderer graphics)
        {
        }

        /// <inheritdoc />
        public override void HandleEvent(GameEvent theEvent)
        {
        }

        /// <inheritdoc />
        public override void Update()
        {
            if (!Services.Input.KeyAvailable())
            {
                return;
            }

            ConsoleKey inputKey = Services.Input.ReadKey();

            if (inputKey == this.keyMapping[(int)Direction.Up])
            {
                if (this.Owner.Velocity.Y != 1)
                {
                    // Can only move up when not moving down
                    this.Owner.Velocity = new Vector2(0, -1);
                }
            }
            else if (inputKey == this.keyMapping[(int)Direction.Down])
            {
                if (this.Owner.Velocity.Y != 1)
                {
                    // Only move down if we aren't moving up
                    this.Owner.Velocity = new Vector2(0, 1);
                }
            }
            else if (inputKey == this.keyMapping[(int)Direction.Left])
            {
                if (this.Owner.Velocity.X != -1)
                {
                    this.Owner.Velocity = new Vector2(-1, 0);
                }
            }
            else if (inputKey == this.keyMapping[(int)Direction.Right])
            {
                if (this.Owner.Velocity.X != 1)
                {
                    // only move right if we aren't moving right
                    this.Owner.Velocity = new Vector2(1, 0);
                }
            }
        }
    }
}