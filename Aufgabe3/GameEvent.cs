﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameEvent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the GameEvent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    /// <summary>
    /// An event used for communication between objects.
    /// </summary>
    public class GameEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameEvent"/> class.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public GameEvent(Entity sender, EventType type, object data)
        {
            this.Sender = sender;
            this.Type = type;
            this.Data = data;
        }

        /// <summary>
        /// Gets or sets the data stored in the event.
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        public Entity Sender { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public EventType Type { get; set; }
    }
}