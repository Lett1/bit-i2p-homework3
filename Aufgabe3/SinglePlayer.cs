﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SinglePlayer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the SinglePlayer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Aufgabe3.Entities;
    using Aufgabe3.Enums;
    using Aufgabe3.Structs;

    /// <summary>
    /// Implements the rules for a singleplayer game of snake.
    /// </summary>
    public class SinglePlayer : GameMode
    {
        /// <summary>
        /// Maximum time between item spawns.
        /// </summary>
        private const int SpawncooldownMax = 600;

        /// <summary>
        /// Minimum time between item spawns.
        /// </summary>
        private const int SpawncooldownMin = 300;

        /// <summary>
        /// The player 1 keysmapping.
        /// </summary>
        private static readonly ConsoleKey[] Player1Keys =
        { 
            ConsoleKey.UpArrow, ConsoleKey.DownArrow, ConsoleKey.LeftArrow, ConsoleKey.RightArrow 
        };

        /// <summary>
        /// The score multiplier.
        /// </summary>
        private int multiplier;

        /// <summary>
        /// The score.
        /// </summary>
        private int score;

        /// <summary>
        /// The snake length.
        /// </summary>
        private int snakeLength;

        /// <summary>
        /// The time till the next object spawn.
        /// </summary>
        private int spawnCooldown;

        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePlayer"/> class.
        /// </summary>
        public SinglePlayer()
        {
            this.score = 0;
            this.snakeLength = 2;
            this.multiplier = 1;
            this.spawnCooldown = Utilities.Random.Next(SpawncooldownMin, SpawncooldownMax + 1);
        }

        /// <inheritdoc />
        public override Rectangle CalculateBounds(Game game)
        {
            Vector2 windowSize = game.Graphics.GetWindowBounds();
            return new Rectangle(new Vector2(1, 3), windowSize.X - 4, windowSize.Y - 5);
        }

        /// <inheritdoc />
        public override void ChangeMultipier(int playerNumber, int amount)
        {
            this.multiplier += amount;
        }

        /// <inheritdoc />
        public override void ChangeScore(int playerNumber, int amount)
        {
            this.score += amount * this.multiplier;
            Debug.WriteLine($"Score: {this.score}");
        }

        /// <inheritdoc />
        public override void ChangeSnakeLength(int playerNumber, int amount)
        {
            this.snakeLength += amount;
        }

        /// <inheritdoc />
        public override bool CheckForGameover(List<Entity> world)
        {
            if (CountObjects(world, EntityType.Snake) == 0)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        public override List<Entity> CreateWorld(Game game)
        {
            List<Entity> world = new List<Entity>();

            var fruit = new Fruit(game);
            fruit.Position = GetRandomPosition(game.ArenaBounds);
            world.Add(fruit);

            var snake = new Snake(game, 's', Player1Keys);
            snake.Position = new Vector2(game.ArenaBounds.Width / 2, (game.ArenaBounds.Height / 2) + 4);
            snake.Velocity.X = 1;

            world.Add(snake);

            return world;
        }

        /// <inheritdoc />
        public override void DrawHud(Game game)
        {
            game.Graphics.PutString(0, 0, $"Score: {this.score}");
            game.Graphics.PutString(0, 1, $"Length: {this.snakeLength}");

            int x = game.ArenaBounds.Position.X - 1;
            int y = game.ArenaBounds.Position.Y - 1;

            int width = game.ArenaBounds.Width + 3;
            int height = game.ArenaBounds.Height + 2;

            // Draw the two horizontal lines
            for (int i = 0; i < width + 1; i++)
            {
                game.Graphics.PutChar(x + i, y, '═');
                game.Graphics.PutChar(x + i, y + height, '═');
            }

            // Draw the two vertical lines
            for (int i = 1; i < height; i++)
            {
                game.Graphics.PutChar(x, y + i, '║');
                game.Graphics.PutChar(x + width, y + i, '║');
            }

            // Draw corners
            game.Graphics.PutChar(x, y, '╔');
            game.Graphics.PutChar(x + width, y, '╗');
            game.Graphics.PutChar(x, y + height, '╚');
            game.Graphics.PutChar(x + width, y + height, '╝');
        }

        /// <inheritdoc />
        public override int GetPlayerNumber(Entity player)
        {
            return 1;
        }

        /// <inheritdoc />
        public override void ShowGameoverScreen(Game game)
        {
            int middleX = game.ArenaBounds.Width / 2;
            int middleY = (game.ArenaBounds.Height / 2) + 4;

            Services.Graphics.PutString(middleX - 4, middleY, "GAME OVER");
            Services.Graphics.PutString(middleX - 13, middleY + 1, "PRESS ANY KEY TO CONTINUE");

            Services.Input.WaitForKeyPress();
        }

        /// <inheritdoc />
        public override void SpawnObjects(Game game, List<Entity> world)
        {
            if (CountObjects(world, EntityType.Fruit) <= 0)
            {
                var fruit = new Fruit(game);
                fruit.Position = GetRandomPosition(game.ArenaBounds);
                world.Add(fruit);
            }

            // Check if its time to spawn a new object, only do it if there are less than 10 objects in the world
            if (this.spawnCooldown <= 0 && world.Count < 10)
            {
                this.spawnCooldown = Utilities.Random.Next(SpawncooldownMin, SpawncooldownMax + 1);

                Entity newObject = new Entity(game, EntityType.None);

                switch (Utilities.Random.Next(0, 4))
                {
                    case 0:
                        newObject = new Colorizer(game);
                        break;
                    case 1:
                        newObject = new FlyAgaric(game);
                        break;
                    case 2:
                        newObject = new MagicMushroom(game);
                        break;
                    case 3:
                        newObject = new Remover(game);
                        break;
                }

                newObject.Position = GetRandomPosition(game.ArenaBounds);
                world.Add(newObject);
            }
            else
            {
                this.spawnCooldown--;
            }
        }

        /// <summary>
        /// Count objects in the game world by type.
        /// </summary>
        /// <param name="world">
        /// The world.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The count.
        /// </returns>
        private static int CountObjects(List<Entity> world, EntityType type)
        {
            int count = 0;

            foreach (Entity entity in world)
            {
                if (entity.Type == type)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Get a random position in the playfield.
        /// </summary>
        /// <param name="bounds">
        /// The bounds.
        /// </param>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        private static Vector2 GetRandomPosition(Rectangle bounds)
        {
            Vector2 position = new Vector2(0, 0)
                                   {
                                       X = Utilities.Random.Next(bounds.Position.X + 1, bounds.Width - 1),
                                       Y = Utilities.Random.Next(bounds.Position.Y + 1, bounds.Height - 1)
                                   };
            return position;
        }
    }
}