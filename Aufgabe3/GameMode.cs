﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameMode.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the GameMode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System.Collections.Generic;

    using Aufgabe3.Structs;

    /// <summary>
    /// Defines the rules of a game.
    /// </summary>
    public abstract class GameMode
    {
        /// <summary>
        /// Calculate the bounds of the playfield.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        /// <returns>
        /// A <see cref="Rectangle"/>.
        /// </returns>
        public abstract Rectangle CalculateBounds(Game game);

        /// <summary>
        /// Change a players score multiplier.
        /// </summary>
        /// <param name="playerNumber">
        /// The player number.
        /// </param>
        /// <param name="amount">
        /// The amount.
        /// </param>
        public abstract void ChangeMultipier(int playerNumber, int amount);

        /// <summary>
        /// Change a players score.
        /// </summary>
        /// <param name="playerNumber">
        /// The player number.
        /// </param>
        /// <param name="amount">
        /// The amount.
        /// </param>
        public abstract void ChangeScore(int playerNumber, int amount);

        /// <summary>
        /// Change the snakes length.
        /// </summary>
        /// <param name="playerNumber">
        /// The player number.
        /// </param>
        /// <param name="amount">
        /// The amount.
        /// </param>
        public abstract void ChangeSnakeLength(int playerNumber, int amount);

        /// <summary>
        /// Check if the game is over.
        /// </summary>
        /// <param name="world">
        /// The world.
        /// </param>
        /// <returns>
        /// True of the game is done.
        /// </returns>
        public abstract bool CheckForGameover(List<Entity> world);

        /// <summary>
        /// Create and populate the game world.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        /// <returns>
        /// A List of Entities that makes up the world.
        /// </returns>
        public abstract List<Entity> CreateWorld(Game game);

        /// <summary>
        /// Draw the HUD.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public abstract void DrawHud(Game game);

        /// <summary>
        /// Get the player number for the given entity.
        /// </summary>
        /// <param name="player">
        /// The player.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public abstract int GetPlayerNumber(Entity player);

        /// <summary>
        /// Show the game over screen.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public abstract void ShowGameoverScreen(Game game);

        /// <summary>
        /// Spawn objects into the world.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        /// <param name="world">
        /// The world.
        /// </param>
        public abstract void SpawnObjects(Game game, List<Entity> world);
    }
}