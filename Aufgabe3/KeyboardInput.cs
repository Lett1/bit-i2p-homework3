﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyboardInput.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the KeyboardInput type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System;

    using Aufgabe3.Interfaces;

    /// <summary>
    /// Gets input from the keyboard.
    /// </summary>
    public class KeyboardInput : IInputManager
    {
        /// <inheritdoc />
        public bool KeyAvailable()
        {
            return Console.KeyAvailable;
        }

        /// <inheritdoc />
        public ConsoleKey ReadKey()
        {
            return Console.ReadKey(true).Key;
        }

        /// <inheritdoc />
        public void WaitForKeyPress(params ConsoleKey[] keys)
        {
            while (true)
            {
                ConsoleKey key = Console.ReadKey(true).Key;
                if (Array.IndexOf(keys, key) > -1 || keys.Length == 0)
                {
                    break;
                }
            }
        }
    }
}