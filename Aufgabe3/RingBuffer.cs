﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RingBuffer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the RingBuffer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System;

    /// <summary>
    /// Implements a queue backed by a ring buffer (which is an array).
    /// </summary>
    public class RingBuffer
    {
        /// <summary>
        /// The buffer.
        /// </summary>
        private readonly GameEvent[] data;

        /// <summary>
        /// The head of the queue.
        /// </summary>
        private int head;

        /// <summary>
        /// The tail of the queue.
        /// </summary>
        private int tail;

        /// <summary>
        /// Initializes a new instance of the <see cref="RingBuffer"/> class.
        /// </summary>
        /// <param name="size">
        /// The size.
        /// </param>
        public RingBuffer(int size)
        {
            this.data = new GameEvent[size];
            this.head = 0;
            this.tail = 0;
        }

        /// <summary>
        /// Check whether the buffer is empty.
        /// </summary>
        /// <returns>
        /// True if there are no elements in the buffer.
        /// </returns>
        public bool IsEmpty()
        {
            return this.head == this.tail;
        }

        /// <summary>
        /// Pop the head of the queue and return it.
        /// </summary>
        /// <returns>
        /// The <see cref="GameEvent"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception of the read would return garbage.
        /// </exception>
        public GameEvent Pop()
        {
            if (!this.IsEmpty())
            {
                GameEvent poppedValue = this.data[this.head];
                this.data[this.head] = null;
                this.head = (this.head + 1) % this.data.Length;
                return poppedValue;
            }
            else
            {
                throw new Exception("Attemped read beyond buffer.");
            }
        }

        /// <summary>
        /// Push new data into the queue.
        /// </summary>
        /// <param name="value">
        /// The value to push.
        /// </param>
        public void Push(GameEvent value)
        {
            this.data[this.tail] = value;
            this.tail = (this.tail + 1) % this.data.Length;
        }
    }
}