﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventType.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the EventType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    /// <summary>
    /// Represents the different types of events.
    /// </summary>
    public enum EventType
    {
        /// <summary>
        /// Increase speed.
        /// </summary>
        SpeedUp,

        /// <summary>
        /// Decrease speed.
        /// </summary>
        SpeedDown,

        /// <summary>
        /// Gain a tail segment.
        /// </summary>
        GainTail,

        /// <summary>
        /// Lose a tail segment.
        /// </summary>
        LoseTail,

        /// <summary>
        /// Change the entity's symbol.
        /// </summary>
        ChangeSymbol,

        /// <summary>
        /// The entity was moved.
        /// </summary>
        EntityWasMoved,

        /// <summary>
        /// Change the entity's color.
        /// </summary>
        ChangeColor,

        /// <summary>
        /// Deal damage to an entity.
        /// </summary>
        TakeDamage,

        /// <summary>
        /// Change the entitie's score.
        /// </summary>
        ChangeScore,

        /// <summary>
        /// Change the entitie's multiplier.
        /// </summary>
        ChangeMultiplier,

        /// <summary>
        /// Change a snake's length.
        /// </summary>
        ChangeSnakeLength,
    }
}