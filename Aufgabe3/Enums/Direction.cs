﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Direction.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Direction type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Enums
{
    /// <summary>
    /// Represents the four cardinal directions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Straight up.
        /// </summary>
        Up = 0,

        /// <summary>
        /// Straight down.
        /// </summary>
        Down,

        /// <summary>
        /// To the left.
        /// </summary>
        Left,

        /// <summary>
        /// To the right.
        /// </summary>
        Right
    }
}