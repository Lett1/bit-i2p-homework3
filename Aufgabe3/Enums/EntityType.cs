﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntityType.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the EntityType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Enums
{
    /// <summary>
    /// The different types of entities. Poor mans typeof().
    /// </summary>
    public enum EntityType
    {
        /// <summary>
        /// Nothing. The initial value.
        /// </summary>
        None,

        /// <summary>
        /// The snake.
        /// </summary>
        Snake,

        /// <summary>
        /// The fruit.
        /// </summary>
        Fruit,

        /// <summary>
        /// The magic mushroom.
        /// </summary>
        MagicMushroom,

        /// <summary>
        /// The remover.
        /// </summary>
        Remover,

        /// <summary>
        /// The fly agaric.
        /// </summary>
        FlyAgaric,

        /// <summary>
        /// The colorizer.
        /// </summary>
        Colorizer
    }
}