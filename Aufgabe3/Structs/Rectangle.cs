﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Rectangle.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Rectangle type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Structs
{
    /// <summary>
    /// A rectangle as defined by a point in the upper left, its width and its height.
    /// </summary>
    public struct Rectangle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> struct.
        /// </summary>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="width">
        /// The width.
        /// </param>
        /// <param name="height">
        /// The height.
        /// </param>
        public Rectangle(Vector2 position, int width, int height)
        {
            this.Position = position;
            this.Width = width;
            this.Height = height;
        }

        /// <summary>
        /// Gets or sets the position of the rectangle.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Check whether a point lies inside the rectangle.
        /// </summary>
        /// <param name="point">
        /// The point.
        /// </param>
        /// <returns>
        /// True if the point was inside, otherwise returns false.
        /// </returns>
        public bool PointInside(Vector2 point)
        {
            if (point.X < this.Position.X || point.X > this.Position.X + this.Width || point.Y < this.Position.Y
                || point.Y > this.Position.Y + this.Height)
            {
                return false;
            }

            return true;
        }
    }
}