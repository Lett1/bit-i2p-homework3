// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TailSegment.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the TailSegment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Components
{
    using System;

    /// <summary>
    /// The tail segment.
    /// </summary>
    public struct TailSegment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TailSegment"/> struct.
        /// </summary>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="color">
        /// The color.
        /// </param>
        public TailSegment(Vector2 position, ConsoleColor color = ConsoleColor.White)
        {
            this.Color = color;
            this.Position = position;
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        public ConsoleColor Color { get; set; }
    }
}