﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Game.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Game type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    using Aufgabe3.Interfaces;
    using Aufgabe3.Structs;

    /// <summary>
    /// An instance of a game of snake.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// The events queue.
        /// </summary>
        private readonly RingBuffer events;

        /// <summary>
        /// A List of Entities that make up the world.
        /// </summary>
        private readonly List<Entity> world;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="mode">
        /// The game mode to use.
        /// </param>
        public Game(GameMode mode)
        {
            // Create event queue
            this.events = new RingBuffer(20);

            // Init graphics adapter
            this.Graphics = new ConsoleRenderer();

            // Init Game mode
            this.Mode = mode;

            // Calculate playfield size
            this.ArenaBounds = this.Mode.CalculateBounds(this);

            // Create world
            this.world = this.Mode.CreateWorld(this);
        }

        /// <summary>
        /// Gets or sets the arena bounds.
        /// </summary>
        public Rectangle ArenaBounds { get; set; }

        /// <summary>
        /// Gets or sets the graphics.
        /// </summary>
        public IRenderer Graphics { get; set; }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        public GameMode Mode { get; set; }

        /// <summary>
        /// Add an event to the game's queue.
        /// </summary>
        /// <param name="gameEvent">
        /// The game event.
        /// </param>
        public void BroadcastEvent(GameEvent gameEvent)
        {
            this.events.Push(gameEvent);
        }

        /// <summary>
        /// Run the game.
        /// </summary>
        public void Run()
        {
            while (!this.Mode.CheckForGameover(this.world))
            {
                this.Mode.SpawnObjects(this, this.world);

                // Update each entity
                foreach (Entity entity in this.world)
                {
                    entity.Update();
                }

                this.ProcessEvents();

                // Remove dead entities
                for (int index = this.world.Count - 1; index >= 0; index--)
                {
                    Entity entity = this.world[index];

                    if (!entity.Alive)
                    {
                        this.world.RemoveAt(index);
                    }
                }

                Thread.Sleep(20);
            }

            this.Mode.ShowGameoverScreen(this);
        }

        /// <summary>
        /// Check if the specified entity hit any other entity.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        private void CheckForCollison(Entity entity)
        {
            List<ICollider> colliders = new List<ICollider>();

            foreach (GameComponent item in entity.Components)
            {
                if (item is ICollider)
                {
                    colliders.Add((ICollider)item);
                }
            }

            // If we have no collision component, we can't collide with anything
            if (colliders.Count <= 0)
            {
                return;
            }

            foreach (ICollider collisionComp in colliders)
            {
                // Check if we hit the border of the console
                if (!this.ArenaBounds.PointInside(entity.Position))
                {
                    collisionComp.OnCollide(null, entity.Position);
                }

                // Check if we collided with something else
                foreach (Entity collider in this.world)
                {
                    if (collisionComp.DoesCollide(collider))
                    {
                        collisionComp.OnCollide(collider, entity.Position);
                    }
                }
            }
        }

        /// <summary>
        /// Process the event queue.
        /// </summary>
        private void ProcessEvents()
        {
            while (!this.events.IsEmpty())
            {
                GameEvent gameEvent = this.events.Pop();
                switch (gameEvent.Type)
                {
                    case EventType.EntityWasMoved:
                        this.CheckForCollison(gameEvent.Sender);

                        Console.Clear();

                        this.Mode.DrawHud(this);

                        // Draw all entities
                        foreach (Entity entity in this.world)
                        {
                            entity.Draw(Services.Graphics);
                        }

                        break;
                    case EventType.ChangeScore:
                        this.Mode.ChangeScore(this.Mode.GetPlayerNumber(gameEvent.Sender), (int)gameEvent.Data);
                        break;
                    case EventType.ChangeMultiplier:
                        this.Mode.ChangeMultipier(this.Mode.GetPlayerNumber(gameEvent.Sender), (int)gameEvent.Data);
                        break;
                    case EventType.ChangeSnakeLength:
                        this.Mode.ChangeSnakeLength(this.Mode.GetPlayerNumber(gameEvent.Sender), (int)gameEvent.Data);
                        break;
                }
            }
        }
    }
}