﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MagicMushroom.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the MagicMushroom type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Entities
{
    using Aufgabe3.Components;
    using Aufgabe3.Enums;

    /// <summary>
    /// Trippy shroom. Increases the speed and modifier of the snake by one.
    /// </summary>
    public class MagicMushroom : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MagicMushroom"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public MagicMushroom(Game game)
            : base(game, EntityType.MagicMushroom)
        {
            this.AddComponent(new CharacterComponent('M'));
            this.AddComponent(new CollisionComponent(true));
        }
    }
}