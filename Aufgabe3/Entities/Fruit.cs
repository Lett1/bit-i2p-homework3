﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Fruit.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Fruit type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Entities
{
    using System;

    using Aufgabe3.Components;
    using Aufgabe3.Enums;

    /// <summary>
    /// A tasty fruit. Gives the snake an extra segment and two points.
    /// </summary>
    public class Fruit : Entity
    {
        /// <summary>
        /// The apple colors.
        /// </summary>
        private static readonly ConsoleColor[] AppleColors =
            { 
            ConsoleColor.Green, ConsoleColor.Red, ConsoleColor.Yellow 
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="Fruit"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public Fruit(Game game)
            : base(game, EntityType.Fruit)
        {
            this.AddComponent(new CharacterComponent('a', AppleColors[Utilities.Random.Next(0, AppleColors.Length)]));
        }
    }
}