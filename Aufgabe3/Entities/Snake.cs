﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Snake.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Snake type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Entities
{
    using System;

    using Aufgabe3.Components;
    using Aufgabe3.Enums;

    /// <summary>
    /// A snake. Slithers across the screen and eats things.
    /// </summary>
    public class Snake : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Snake"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        /// <param name="character">
        /// The character for the snakes head.
        /// </param>
        /// <param name="keyBindings">
        /// The key bindings.
        /// </param>
        public Snake(Game game, char character, ConsoleKey[] keyBindings)
            : base(game, EntityType.Snake)
        {
            this.AddComponent(new CharacterComponent(character));
            this.AddComponent(new SnakeColliderComponent());
            this.AddComponent(new KeyboardInputComponent(keyBindings));
            this.AddComponent(new TailComponent());
            this.AddComponent(new BasicMovementComponent(15));
        }
    }
}