﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlyAgaric.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the FlyAgaric type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Entities
{
    using Aufgabe3.Components;
    using Aufgabe3.Enums;

    /// <summary>
    /// Deadly mushroom. Slows the snake down and decreases the multiplier.
    /// </summary>
    public class FlyAgaric : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FlyAgaric"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public FlyAgaric(Game game)
            : base(game, EntityType.FlyAgaric)
        {
            this.AddComponent(new CharacterComponent('F'));
            this.AddComponent(new CollisionComponent(true));
        }
    }
}