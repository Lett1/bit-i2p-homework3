﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Remover.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Remover type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Entities
{
    using Aufgabe3.Components;
    using Aufgabe3.Enums;

    /// <summary>
    /// Removes the last segment of the snake and 10 Points.
    /// </summary>
    public class Remover : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Remover"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public Remover(Game game)
            : base(game, EntityType.Remover)
        {
            this.AddComponent(new CharacterComponent('Ω'));
            this.AddComponent(new CollisionComponent(true));
        }
    }
}