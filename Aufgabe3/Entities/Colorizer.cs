﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Colorizer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Colorizer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Entities
{
    using Aufgabe3.Components;
    using Aufgabe3.Enums;

    /// <summary>
    /// A rainbow of color. Colors the snake once it moves through it. Dissappears after 10 colorings.
    /// </summary>
    public class Colorizer : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Colorizer"/> class.
        /// </summary>
        /// <param name="game">
        /// The game.
        /// </param>
        public Colorizer(Game game)
            : base(game, EntityType.Colorizer)
        {
            this.AddComponent(new CharacterComponent('R'));
            this.AddComponent(new CollisionComponent(true));
            this.AddComponent(new ColorCycleComponent());
            this.AddComponent(new DurabilityComponent(10));
        }
    }
}