﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Multiplayer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the MultiPlayer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System;
    using System.Collections.Generic;

    using Aufgabe3.Entities;
    using Aufgabe3.Enums;
    using Aufgabe3.Structs;

    /// <summary>
    /// Implements a 2 player game of snake.
    /// </summary>
    public class MultiPlayer : GameMode
    {
        /// <summary>
        /// Maximum time between item spawns.
        /// </summary>
        private const int SpawncooldownMax = 600;

        /// <summary>
        /// Minimum time between item spawns.
        /// </summary>
        private const int SpawncooldownMin = 300;

        /// <summary>
        /// Keybindings for player 1.
        /// </summary>
        private static readonly ConsoleKey[] Player1Keys =
        { 
            ConsoleKey.UpArrow, ConsoleKey.DownArrow, ConsoleKey.LeftArrow, ConsoleKey.RightArrow 
        };

        /// <summary>
        /// Keybindings for player 2.
        /// </summary>
        private static readonly ConsoleKey[] Player2Keys = { ConsoleKey.W, ConsoleKey.S, ConsoleKey.A, ConsoleKey.D };

        /// <summary>
        /// Holds three ints for the score of each player: score, length, multiplier
        /// </summary>
        private readonly int[,] scoreBoard;

        /// <summary>
        /// Holds a reference to player 1.
        /// </summary>
        private Snake player1;

        /// <summary>
        /// Holds a reference to player 2.
        /// </summary>
        private Snake player2;

        /// <summary>
        /// How long till the next item spawn.
        /// </summary>
        private int spawnCooldown;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiPlayer"/> class.
        /// </summary>
        public MultiPlayer()
        {
            this.scoreBoard = new int[2, 3] { { 0, 2, 1 }, { 0, 2, 1 } };
            this.spawnCooldown = Utilities.Random.Next(SpawncooldownMin, SpawncooldownMax + 1);
        }

        /// <inheritdoc />
        public override Rectangle CalculateBounds(Game game)
        {
            Vector2 windowSize = game.Graphics.GetWindowBounds();
            return new Rectangle(new Vector2(1, 3), windowSize.X - 4, windowSize.Y - 5);
        }

        /// <inheritdoc />
        public override void ChangeMultipier(int playerNumber, int amount)
        {
            this.scoreBoard[playerNumber, 2] += amount;
        }

        /// <inheritdoc />
        public override void ChangeScore(int playerNumber, int amount)
        {
            this.scoreBoard[playerNumber, 0] += amount * this.scoreBoard[playerNumber, 2];
        }

        /// <inheritdoc />
        public override void ChangeSnakeLength(int playerNumber, int amount)
        {
            this.scoreBoard[playerNumber, 1] += amount;
        }

        /// <inheritdoc />
        public override bool CheckForGameover(List<Entity> world)
        {
            if (CountObjects(world, EntityType.Snake) <= 1)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        public override List<Entity> CreateWorld(Game game)
        {
            List<Entity> world = new List<Entity>();

            Fruit fruit = new Fruit(game);
            fruit.Position = GetRandomPosition(game.ArenaBounds);
            world.Add(fruit);

            Snake snake1 = new Snake(game, '1', Player1Keys);
            snake1.Position = new Vector2(game.ArenaBounds.Position.X + 2, (game.ArenaBounds.Height / 2) + 4);
            snake1.Velocity.X = 1;

            this.player1 = snake1;
            world.Add(snake1);

            Snake snake2 = new Snake(game, '2', Player2Keys);
            snake2.Position = new Vector2(game.ArenaBounds.Width - 2, (game.ArenaBounds.Height / 2) + 4);
            snake2.Velocity.X = -1;

            this.player2 = snake2;
            world.Add(snake2);

            return world;
        }

        /// <inheritdoc />
        public override void DrawHud(Game game)
        {
            game.Graphics.PutString(0, 0, $"Player1: Score:  {this.scoreBoard[0, 0]}");
            game.Graphics.PutString(0, 1, $"         Length: {this.scoreBoard[0, 1]}");

            game.Graphics.PutString(30, 0, $"Player 2: Score:  {this.scoreBoard[1, 0]}");
            game.Graphics.PutString(30, 1, $"          Length: {this.scoreBoard[1, 1]}");

            int x = game.ArenaBounds.Position.X - 1;
            int y = game.ArenaBounds.Position.Y - 1;

            int width = game.ArenaBounds.Width + 3;
            int height = game.ArenaBounds.Height + 2;

            // Draw the two horizontal lines
            for (int i = 0; i < width + 1; i++)
            {
                game.Graphics.PutChar(x + i, y, '═');
                game.Graphics.PutChar(x + i, y + height, '═');
            }

            // Draw the two vertical lines
            for (int i = 1; i < height; i++)
            {
                game.Graphics.PutChar(x, y + i, '║');
                game.Graphics.PutChar(x + width, y + i, '║');
            }

            // Draw corners
            game.Graphics.PutChar(x, y, '╔');
            game.Graphics.PutChar(x + width, y, '╗');
            game.Graphics.PutChar(x, y + height, '╚');
            game.Graphics.PutChar(x + width, y + height, '╝');
        }

        /// <inheritdoc />
        public override int GetPlayerNumber(Entity player)
        {
            if (player == this.player1)
            {
                return 0;
            }
            else if (player == this.player2)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        /// <inheritdoc />
        public override void ShowGameoverScreen(Game game)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override void SpawnObjects(Game game, List<Entity> world)
        {
            if (CountObjects(world, EntityType.Fruit) <= 0)
            {
                Fruit fruit = new Fruit(game);
                fruit.Position = GetRandomPosition(game.ArenaBounds);
                world.Add(fruit);
            }

            // Check if its time to spawn a new object, only do it if there are less than 10 objects in the world
            if (this.spawnCooldown <= 0 && world.Count < 10)
            {
                this.spawnCooldown = Utilities.Random.Next(SpawncooldownMin, SpawncooldownMax + 1);

                Entity newObject = new Entity(game, EntityType.None);

                switch (Utilities.Random.Next(0, 4))
                {
                    case 0:
                        newObject = new Colorizer(game);
                        break;
                    case 1:
                        newObject = new FlyAgaric(game);
                        break;
                    case 2:
                        newObject = new MagicMushroom(game);
                        break;
                    case 3:
                        newObject = new Remover(game);
                        break;
                }

                newObject.Position = GetRandomPosition(game.ArenaBounds);
                world.Add(newObject);
            }
            else
            {
                this.spawnCooldown--;
            }
        }

        /// <inheritdoc />
        private static int CountObjects(List<Entity> world, EntityType type)
        {
            int count = 0;

            foreach (Entity entity in world)
            {
                if (entity.Type == type)
                {
                    count++;
                }
            }

            return count;
        }

        /// <inheritdoc />
        private static Vector2 GetRandomPosition(Rectangle bounds)
        {
            Vector2 position = new Vector2(0, 0)
                                   {
                                       X = Utilities.Random.Next(bounds.Position.X + 1, bounds.Width - 1),
                                       Y = Utilities.Random.Next(bounds.Position.Y + 1, bounds.Height - 1)
                                   };
            return position;
        }
    }
}