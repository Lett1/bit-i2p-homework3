﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInputManager.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the IInputManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Interfaces
{
    using System;

    /// <summary>
    /// The InputManager interface.
    /// </summary>
    public interface IInputManager
    {
        /// <summary>
        /// Whether a key has been pressed but not processed yet.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool KeyAvailable();

        /// <summary>
        /// Read a single Key from input.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleKey"/>.
        /// </returns>
        ConsoleKey ReadKey();

        /// <summary>
        /// Wait for a keypress of one or more specific keys. Or just any key.
        /// </summary>
        /// <param name="keys">
        /// The keys to wait for.
        /// </param>
        void WaitForKeyPress(params ConsoleKey[] keys);
    }
}