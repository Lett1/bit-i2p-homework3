﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICollider.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the ICollider type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3.Interfaces
{
    /// <summary>
    /// The Collider interface. Used by components that provide collision handling.
    /// </summary>
    public interface ICollider
    {
        /// <summary>
        /// Whether the owning entity collides with something.
        /// </summary>
        /// <param name="collider">
        /// The collider.
        /// </param>
        /// <returns>
        /// The result of the check.
        /// </returns>
        bool DoesCollide(Entity collider);

        /// <summary>
        /// This is called when the entity hit something.
        /// </summary>
        /// <param name="collider">
        /// What we collided with.
        /// </param>
        /// <param name="position">
        /// The position of the collision.
        /// </param>
        void OnCollide(Entity collider, Vector2 position);
    }
}