﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Entity.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Entity type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System.Collections.Generic;

    using Aufgabe3.Enums;
    using Aufgabe3.Interfaces;

    /// <summary>
    /// An entity in the game world.
    /// </summary>
    public class Entity
    {
        /// <summary>
        /// The event queue size.
        /// </summary>
        private const int EventQueueSize = 10;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="game">
        /// Instance of the game currently running.
        /// </param>
        /// <param name="type">
        /// The entities type.
        /// </param>
        public Entity(Game game, EntityType type)
        {
            this.Components = new List<GameComponent>();
            this.Children = new List<Entity>();
            this.Position = new Vector2(0, 0);
            this.Velocity = new Vector2(0, 0);
            this.Alive = true;
            this.Events = new RingBuffer(EventQueueSize);
            this.Game = game;
            this.Type = type;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this entity is alive.
        /// </summary>
        public bool Alive { get; set; }

        /// <summary>
        /// Gets or sets the children of this entity.
        /// </summary>
        public List<Entity> Children { get; set; }

        /// <summary>
        /// Gets the components.
        /// </summary>
        public List<GameComponent> Components { get; }

        /// <summary>
        /// Gets the eventsbuffer.
        /// </summary>
        public RingBuffer Events { get; }

        /// <summary>
        /// Gets the game instance associated with the entity.
        /// </summary>
        public Game Game { get; }

        /// <summary>
        /// Gets or sets the parent of the entity.
        /// </summary>
        public Entity Parent { get; set; }

        /// <summary>
        /// Gets or sets the position of the entity.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Gets or sets the type of this entity.
        /// </summary>
        public EntityType Type { get; set; }

        /// <summary>
        /// Gets or sets the velocity of this entity.
        /// </summary>
        public Vector2 Velocity { get; set; }

        /// <summary>
        /// Add a new entity as a child to this one.
        /// </summary>
        /// <param name="ent">
        /// The child.
        /// </param>
        public void AddChildEntity(Entity ent)
        {
            ent.Parent = this;
            this.Children.Add(ent);
        }

        /// <summary>
        /// Add a new component to the entity.
        /// </summary>
        /// <param name="component">
        /// The component.
        /// </param>
        public void AddComponent(GameComponent component)
        {
            component.Owner = this;
            this.Components.Add(component);
        }

        /// <summary>
        /// Destroy the entity.
        /// </summary>
        public void Destroy()
        {
            this.Components.Clear();
            this.Alive = false;
        }

        /// <summary>
        /// Tell all the components and children to draw themselves.
        /// </summary>
        /// <param name="graphics">
        /// The graphics adapter to use.
        /// </param>
        public void Draw(IRenderer graphics)
        {
            foreach (GameComponent component in this.Components)
            {
                component.Draw(graphics);
            }

            foreach (Entity child in this.Children)
            {
                child.Draw(graphics);
            }
        }

        /// <summary>
        /// Process all pending events.
        /// </summary>
        public void ProcessEvents()
        {
        }

        /// <summary>
        /// Update all components, children and process events.
        /// </summary>
        public void Update()
        {
            foreach (GameComponent component in this.Components)
            {
                component.Update();
            }

            foreach (Entity child in this.Children)
            {
                child.Update();
            }

            while (!this.Events.IsEmpty())
            {
                GameEvent gameEvent = this.Events.Pop();

                if (gameEvent.Type == EventType.EntityWasMoved)
                {
                    this.Game.BroadcastEvent(gameEvent);
                }

                foreach (GameComponent component in this.Components)
                {
                    if (component.SubscribedEvents.Contains(gameEvent.Type))
                    {
                        component.HandleEvent(gameEvent);
                    }
                }
            }
        }
    }
}