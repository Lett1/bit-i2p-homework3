﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utilities.cs" company="LettSot">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Utilities type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System;

    /// <summary>
    /// Provides utilities for the rest of the game.
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// The rnd.
        /// </summary>
        private static Random rnd;

        /// <summary>
        /// Gets the Random Number Generator.
        /// </summary>
        public static Random Random
        {
            get
            {
                if (rnd == null)
                {
                    rnd = new Random();
                }

                return rnd;
            }
        }

        /// <summary>
        /// Gets a random color.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleColor"/>.
        /// </returns>
        public static ConsoleColor GetRandomColor()
        {
            return (ConsoleColor)Random.Next(1, 16);
        }
    }
}