﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Vector2.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Vector2 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    /// <summary>
    /// A pair of coordinates on a 2D plane.
    /// </summary>
    public class Vector2
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2"/> class.
        /// </summary>
        /// <param name="x">
        /// The x.
        /// </param>
        /// <param name="y">
        /// The y.
        /// </param>
        public Vector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Gets or sets the x component.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the y component.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Add another vector to this vector.
        /// </summary>
        /// <param name="vector">
        /// The vector.
        /// </param>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        public Vector2 Add(Vector2 vector)
        {
            return new Vector2(this.X + vector.X, this.Y + vector.Y);
        }

        /// <summary>
        /// Check if a vector is equal to this one.
        /// </summary>
        /// <param name="otherVector">
        /// The other vector.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Equals(Vector2 otherVector)
        {
            return this.X == otherVector.X && this.Y == otherVector.Y;
        }

        /// <summary>
        /// Multiplies this vector with a scalar value.
        /// </summary>
        /// <param name="skalar">
        /// The skalar.
        /// </param>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        public Vector2 Mul(int skalar)
        {
            return new Vector2(this.X * skalar, this.Y * skalar);
        }

        /// <summary>
        /// Substracts another vector from this vector.
        /// </summary>
        /// <param name="vector">
        /// The vector.
        /// </param>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        public Vector2 Sub(Vector2 vector)
        {
            return new Vector2(this.X - vector.X, this.Y - vector.Y);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe3.Vector2"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Aufgabe3.Vector2"/>.</returns>
        public override string ToString()
        {
            return string.Format($"{this.X}, {this.Y}");
        }
    }
}