﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameComponent.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the GameComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aufgabe3
{
    using System.Collections.Generic;

    using Aufgabe3.Interfaces;

    /// <summary>
    /// A component that defines an entities behaviour.
    /// </summary>
    public abstract class GameComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameComponent"/> class.
        /// </summary>
        protected GameComponent()
        {
            this.SubscribedEvents = new List<EventType>();
        }

        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        public Entity Owner { get; set; }

        /// <summary>
        /// Gets or sets the subscribed events.
        /// </summary>
        public List<EventType> SubscribedEvents { get; set; }

        /// <summary>
        /// Sends an event to the components owner.
        /// </summary>
        /// <param name="theEvent">
        /// The the event.
        /// </param>
        public virtual void BroadcastEvent(GameEvent theEvent)
        {
            this.Owner.Events.Push(theEvent);
        }

        /// <summary>
        /// Draw this component.
        /// </summary>
        /// <param name="graphics">
        /// The graphics.
        /// </param>
        public abstract void Draw(IRenderer graphics);

        /// <summary>
        /// Handle events.
        /// </summary>
        /// <param name="theEvent">
        /// The the event.
        /// </param>
        public abstract void HandleEvent(GameEvent theEvent);

        /// <summary>
        /// Update this component.
        /// </summary>
        public abstract void Update();
    }
}